import json
from random import randint
from time import sleep
from datetime import datetime

from pyjung import JungTasker


class DeviceGateway:
    """ A device gateway simulator meant to be used in experiments. """

    def __init__(self, kafka_broker, gateway_id):
        self.gateway_id = gateway_id
        self.devices = {}
        self.tasker = JungTasker(in_topic=gateway_id + "_tasks",
                                 out_topic=gateway_id + "_results",
                                 ip=kafka_broker,
                                 group="device-gateway")
        self.consumer = self.tasker.consumer
        self.reading_tasker = JungTasker(in_topic="none",
                                 out_topic="reading_tasks",
                                 ip=kafka_broker,
                                 group="device-gateway")

    def get_commands(self):
        commands = []
        for offset in self.consumer.get_messages(count=10, block=True, timeout=0.03):
            message = json.loads(offset.message.value.decode('utf-8'))
            commands.append(message)
        return commands

    def add_device(self, device):
        self.devices[device["device_id"]] = device

    def send_command(self, device_id, command):
        self.devices[device_id]["state"] = command

    def send_readings(self):
        for device_id, device in self.devices.items():
            reading = {"device_id": device_id,
                       "gateway": self.gateway_id,
                       "reading": device["state"],
                       "timestamp": datetime.now().isoformat()}
            write_task = self.tasker.create_task("WRITE", reading)
            self.reading_tasker.publish(write_task)

    def run(self, messages=10):
        while True:
            if messages > 0:
                messages -= 1
                self.send_readings()
                sleep(1)

            commands = self.get_commands()
            for message in commands:
                command = message["content"]
                task_id = message["id"]
                self.send_command(command["device_id"], command["command"])
                self.tasker.publish({"task_id": task_id, "result":"success"})
