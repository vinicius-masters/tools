#!/usr/bin/python3

import json
import psutil
import docker
import argparse

from multiprocessing import Process
from collections import defaultdict
from subprocess import Popen
from random import randint
from time import sleep

from tools import DeviceGateway, JungClient


class Experiment:

    def __init__(self, conf):
        self.platform = conf["platform"]
        self.api_gateway = conf["apiGateway"]
        self.kafka_broker = conf["kafkaBroker"]
        self.users = conf["users"]
        self.devices_per_user = conf["devicesPerUser"]
        self.device_readings = conf["deviceReadings"]
        self.jung_path = conf["jungPath"]
        self.jung_services = conf["microservices"]

        self.api_client = JungClient(self.api_gateway)
        self.device_gateway = DeviceGateway(self.kafka_broker, "g1")

    def run_jung(self, network_delay=25):
        self.cpu_time_reference = 0
        self.duration = None
        self.instances = {}
        self.iteration_results = {}

        if self.platform == "osv":
            print("running Jung on OSv...")
            run_cmd = ["./run.sh", "KAFKA_BROKER=" + self.kafka_broker]

            for microservice in self.jung_services:
                instance = Popen(run_cmd, cwd=self.jung_path + microservice)
                self.instances[microservice] = psutil.Process(instance.pid)
                sleep(2)

            # give some time for the unikernels to get their network addresses
            sleep(network_delay)

            # this call sets the start reference for psutil
            self.collect_metrics()

        elif self.platform == "docker":
            print("running Jung on Docker...")

            client = docker.from_env()

            for microservice in self.jung_services:
                environ = {"KAFKA_BROKER": self.kafka_broker}
                container = client.containers.run(microservice,
                                                  detach=True,
                                                  environment=environ)
                microservice_pid = int(container.top()["Processes"][1][1])
                self.instances[microservice] = psutil.Process(microservice_pid)

            sleep(network_delay * 0.2)
        else:
            print("running Jung on host OS...")

    def populate(self):
        # create users
        for i in range(self.users):
            username = "user" + str(i)
            password = "senha" + str(i)
            self.api_client.create_user(
                {"username": username, "password": password})

            # add devices to user
            for j in range(self.devices_per_user):
                device_id = "dev" + str(i) + str(j)
                device = {
                    "device_id": device_id,
                    "gateway": self.device_gateway.gateway_id,
                    "type": "light",
                    "username": username,
                    "state": False
                }

                #if reading <condition> value is not True, send command
                rule = {
                    "condition": "==",
                    "target": False,
                    "command": False
                }

                self.device_gateway.add_device(device)
                self.api_client.create_device(username, password, device)
                self.api_client.create_rule(username, password, device_id, rule)

    def run_gateway(self, readings_per_device=10):
        gateway = Process(target=self.device_gateway.run,
                          args=(readings_per_device,))
        gateway.start()
        self.instances["device-gateway"] = psutil.Process(gateway.pid)
        sleep(readings_per_device + 5)

    def process_metrics(self, service, process):
        memory = round(process.memory_info()[0]/(1024 * 1024), 2)
        metrics = {
            "cpu_times": sum(process.cpu_times()),
            "memory": memory
        }

        # this is the desired result, obtained after the reference execution
        if self.duration is not None:
            self.cpu_time_reference = self.iteration_results[service]["cpu_times"]
            metrics["cpu_times"] -= self.cpu_time_reference
            metrics["cpu_times"] /= self.duration

        self.iteration_results[service] = metrics

    def osv_metrics(self):
        for service, proc in self.instances.items():
            if service in self.jung_services:
                for process in proc.children(recursive=True):
                    with process.oneshot():
                        name = process.name()
                        if name not in ["sudo", "capstan"]:
                            self.process_metrics(service, process)

    def docker_metrics(self):
        for service, process in self.instances.items():
            if service in self.jung_services:
                with process.oneshot():
                    self.process_metrics(service, process)

    def collect_metrics(self):
        if self.platform == "osv":
            return self.osv_metrics()
        elif self.platform == "docker":
            return self.docker_metrics()

    def kill_all(self):
        for service, process in self.instances.items():
            print("killing {} {}".format(service, process.pid))
            for proc in process.children(recursive=True):
                proc.kill()
            process.kill()
