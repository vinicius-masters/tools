#!/usr/bin/python3

import json

from time import time

from tools import Experiment
from pyjung import JungTasker


class ExperimentRunner:

    def __init__(self, config_file="runner_config.json"):
        with open(config_file) as _conf:
            conf = json.load(_conf)

            self.kafka_broker = conf["kafkaBroker"]
            self.api_gateway = conf["apiGateway"]
            self.jung_path = conf["jungPath"]

        self.tasker = JungTasker(in_topic="runner",
                                 out_topic="controller",
                                 ip=self.kafka_broker,
                                 group="experiment-runner")
        self.consumer = self.tasker.consumer

    def process_message(self, message):        
        response = {"task_id": message["id"]}
        
        if message["task"] == "RUN":
            conf = message["content"]

            conf["apiGateway"] = self.api_gateway
            conf["jungPath"] = self.jung_path
            conf["kafkaBroker"] = self.kafka_broker
            
            self.experiment = Experiment(conf)
            self.experiment.run_jung()
            self.experiment.populate()
            self.experiment.run_gateway()
            # need to set a time reference for CPU measurements
            self.experiment.collect_metrics()
            self.experiment.start_time = time()

            response["result"] = "READY"

        elif message["task"] == "STOP":
            self.experiment.end_time = time()
            self.experiment.duration = self.experiment.end_time - self.experiment.start_time
            self.experiment.collect_metrics()
            self.experiment.kill_all()

            response["result"] = self.experiment.iteration_results

        return response

    def run(self):
        print("waiting for remote commands...")
        while True:
            for offset_message in self.consumer.get_messages(count=1,
                                                             block=True,
                                                             timeout=0.03):
                message = json.loads(offset_message.message.value)
                try:
                    response = self.process_message(message)

                except Exception as e:
                    print("exception:", e)
                    response = {"error": "malformed message"}

                print("response:\n {}".format(response))
                self.tasker.publish(response)


def main():
    runner = ExperimentRunner()
    runner.run()


if __name__ == "__main__":
    main()
