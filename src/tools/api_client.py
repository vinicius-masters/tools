import statistics
import requests
import json

from time import sleep, time


class JungClient:

    def __init__(self, api_gateway):
        self.api_gateway = api_gateway
        self.users_url = "http://" + self.api_gateway + "/users"
        self.latencies = []

    def make_headers(self, password):
        return {"password": password}

    def create_user(self, user):
        return requests.post(self.users_url, json=user)

    def create_device(self, username, password, device):
        headers = self.make_headers(password)
        return requests.post("/".join([self.users_url, username, "devices"]),
                             json=device, headers=headers)

    def get_readings(self, username, password, device_id):
        headers = self.make_headers(password)
        return requests.get("/".join([self.users_url, username, device_id, "readings"]),
                            headers=headers)

    def create_rule(self, username, password, device_id, rule):
        headers = self.make_headers(password)
        endpoint = "/".join([self.users_url, username, "devices", device_id, "rules"])
        return requests.post(endpoint, json=rule, headers=headers)

    def send_command(self, username, password, device_id, command):
        headers = self.make_headers(password)
        return requests.post("/".join([self.users_url, username, device_id, "commands"]),
                             json=command, headers=headers)

    def get_latency_summary(self, queries):
        latencies = []
        start = time()
        for query in queries:
            headers = self.make_headers(query[1])
            url = "/".join([self.users_url, query[0]])
            response = requests.get(url, headers=headers)
            latencies.append(response.elapsed.microseconds * 0.001)

        duration = time() - start
        reqs = len(queries)

        summary = {
            "avgLatency": statistics.mean(latencies),
            "stdDeviation": statistics.pstdev(latencies),
            "variance": statistics.pvariance(latencies),
            "duration": duration,
            "requests": len(queries),
            "throughput": reqs/duration
        }

        return summary


def main():
    client = JungClient("localhost:33000")
    queries = [("user0", "senha0") for i in range(0, 100)]
    summary = client.get_latency_summary(queries)
    print(json.dumps(summary, indent=4))


if __name__ == "__main__":
    main()
