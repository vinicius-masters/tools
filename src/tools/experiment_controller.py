#!/usr/bin/python3

import json

from collections import defaultdict
from statistics import mean, stdev
from subprocess import Popen
from time import sleep

from pyjung import JungTasker


class ExperimentController:

    def __init__(self, config_file="controller_config.json"):     
        with open(config_file) as _conf:
            conf = json.load(_conf)

            self.kafka_broker = conf["kafkaBroker"]
            self.platforms = conf["platforms"]
            self.iterations = conf["iterations"]
            self.parameters = conf["parameters"]
            self.jmeter_config = conf["jmeter"]
            self.results_path = conf["resultsPath"]

        self.tasker = JungTasker(in_topic="controller",
                                 out_topic="runner",
                                 ip=self.kafka_broker,
                                 group="experiment-controller")
        #holds results from multiple iterations to be summarised in the end
        self.results = defaultdict(lambda: {"cpu_times": [], "memory": []})

    def run_jmeter(self, output_name):
        output_file = self.results_path + output_name + ".jtl"
        cmd = ["jmeter", "-n", "-t", self.jmeter_config, "-l", output_file]
        jmeter = Popen(cmd)
        jmeter.wait()

    def summarise_results(self, platform):
        for service, results in self.results.items():
            for key, value in results.items():
                self.results[service][key] = {"mean": mean(value),
                                              "stdev": stdev(value)}

        with open(self.results_path + platform + "_results.json", "w") as output:
            json.dump(self.results, output, indent=4)

        self.results = defaultdict(lambda: {"cpu_times": [], "memory": []})

    def run(self):
        for platform in self.platforms:
            self.parameters["platform"] = platform
            
            for _ in range(self.iterations):
                run_task = self.tasker.create_task("RUN", self.parameters)
                self.tasker.publish(run_task)
                              
                response = None
                while response is None:
                    response = self.tasker.get_task_result(run_task["id"])
                
                if response == "READY":
                    self.run_jmeter(platform)
                    
                    #stop remote execution and expect metrics in response
                    stop_task = self.tasker.create_task("STOP", None)
                    self.tasker.publish(stop_task)

                    iteration_results = self.tasker.get_task_result(stop_task["id"])

                    for service, result in iteration_results.items():
                        for metric, value in result.items():
                            self.results[service][metric].append(value)

            self.summarise_results(platform)
        

def main():
    client = ExperimentController()
    client.run()


if __name__ == "__main__":
    main()