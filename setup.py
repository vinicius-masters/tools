from setuptools import setup
from setuptools import find_packages

from glob import glob
from os.path import basename
from os.path import splitext

setup(name='jungtools',
      version='0.1',
      author='Vinícius Fraga',
      author_email='vinicius.vmsf@gmail.com',
      packages=find_packages('src'),
      package_dir={'': 'src'},
      py_modules=[splitext(basename(path))[0] for path in glob('src/*.py')],
      install_requires=[],
     )